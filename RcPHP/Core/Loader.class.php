<?php
/**
 * Loader class file.
 *
 * @author         RcPHP Dev Team
 * @copyright      Copyright (c) 2013,RcPHP Dev Team
 * @license        Apache License 2.0 {@link http://www.apache.org/licenses/LICENSE-2.0}
 * @package        Core
 * @since          1.0
 */
namespace RCPHP;

defined('IN_RCPHP') or exit('Access denied');

class Loader
{

	/**
	 * Namespace
	 *
	 * @var array
	 */
	public static $namespaces = array();

	/**
	 * 注册自动加载函数
	 *
	 * @return void
	 */
	public static function registerAutoloader()
	{
		/**
		 * Register system namespace.
		 */
		self::registerNamespace("RCPHP", RCPHP_PATH . "Core");
		self::registerNamespace("RCPHP\\Cache", RCPHP_PATH);
		self::registerNamespace("RCPHP\\Crypt", RCPHP_PATH . "Crypt");
		self::registerNamespace("RCPHP\\DB", RCPHP_PATH . "Db");
		self::registerNamespace("RCPHP\\Net", RCPHP_PATH . "Net");
		self::registerNamespace("RCPHP\\Util", RCPHP_PATH . "Util");
		self::registerNamespace("RCPHP\\Storage", RCPHP_PATH . "Storage");

		spl_autoload_register('RCPHP\Loader::autoload');
	}

	/**
	 * Add namespace
	 *
	 * @param string $name
	 * @param string $path
	 * @return void
	 */
	public static function registerNamespace($name, $path)
	{
		self::$namespaces[$name] = $path;
	}

	/**
	 * 自动加载 __autoload
	 *
	 * @param string $class
	 * @return void
	 */
	private static function autoload($class)
	{
		$tmp = $root = explode('\\', $class);

		array_pop($tmp);

		$tmp = implode("\\", $tmp);

		$className = end($root);

		if(count($root) > 1 && isset(self::$namespaces[$tmp]))
		{
			unset($root[count($root) - 1]);
			$namespace = implode("\\", $root);
			RcPHP::loadFile(self::$namespaces[$namespace] . DS . str_replace('\\', '/', $className) . ".class.php");
		}
		elseif(file_exists(CONTROLLER_PATH . $className . ".class.php"))
		{
			RcPHP::loadFile(CONTROLLER_PATH . $className . ".class.php");
		}
		elseif(file_exists(MODEL_PATH . $className . ".class.php"))
		{
			RcPHP::loadFile(MODEL_PATH . $className . ".class.php");
		}
		else
		{
			Controller::halt('The file of class ' . $class . ' is not exists!');
		}
	}
}