<?php
/**
 * Check class file.
 *
 * @author         RcPHP Dev Team
 * @copyright      Copyright (c) 2013,RcPHP Dev Team
 * @license        Apache License 2.0 {@link http://www.apache.org/licenses/LICENSE-2.0}
 * @package        Util
 * @since          1.0
 */
namespace RCPHP\Util;

defined('IN_RCPHP') or exit('Access denied');

class Check
{

	/**
	 * Check string is Email.
	 *
	 * @param string $str
	 * @return bool
	 */
	public static function isEmail($str)
	{
		return filter_var($str, FILTER_VALIDATE_EMAIL);
	}

	/**
	 * Check string is URL.
	 *
	 * @param string $str
	 * @return bool
	 */
	public static function isUrl($str)
	{
		return filter_var($str, FILTER_VALIDATE_URL);
	}

	/**
	 * Check string is contains illegal characters.
	 *
	 * @param string $str
	 * @return bool
	 */
	public static function isInvalidStr($str)
	{
		if(empty($str))
		{
			return false;
		}

		return preg_match('#[!#$%^&*(){}~`"\';:?+=<>/\[\]]+#', $str) ? $str : false;
	}

	/**
	 * Check number is post code.
	 *
	 * @param int $num
	 * @return bool
	 */
	public static function isPostNum($num)
	{
		$num = intval($num);

		if(!$num)
		{
			return false;
		}

		return preg_match('#^[1-9][0-9]{5}$#', $num) ? $num : false;
	}

	/**
	 * Check num is ID Number.
	 *
	 * @param int|string $num
	 * @return bool
	 */
	public static function isPersonalCard($num)
	{
		if(empty($num))
		{
			return false;
		}

		return preg_match('#^[\d]{15}$|^[\d]{18}$#', $num) ? $num : false;
	}

	/**
	 * Check string is ipv4.
	 *
	 * @param string $str
	 * @return bool
	 */
	public static function isIpv4($str)
	{
		return filter_var($str, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);
	}

	/**
	 * Check string is ipv6.
	 *
	 * @param string $str
	 * @return bool
	 */
	public static function isIpv6($str)
	{
		return filter_var($str, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6);
	}

	/**
	 * Check string is ISBN.
	 *
	 * @param string $str
	 * @return bool
	 */
	public static function isBookIsbn($str)
	{
		if(empty($str))
		{
			return false;
		}

		return preg_match('#^978[\d]{10}$|^978-[\d]{10}$#', $str) ? $str : false;
	}

	/**
	 * Check number is mobile.
	 *
	 * @param int $num
	 * @return bool
	 */
	public static function isMobile($num)
	{
		$num = intval($num);

		if(!$num)
		{
			return false;
		}

		return preg_match('#^(13|14|15|18|17)[0-9]{9}$#', $num) ? $num : false;
	}

	/**
	 * Check string is phone.
	 *
	 * @param string $str
	 * @return bool
	 */
	public static function isTel($str)
	{
		if(empty($str))
		{
			return false;
		}

		return preg_match('#^\d{3}-\d{7,8}|\d{4}-\d{7,8}$#', $str) ? $str : false;
	}

	/**
	 * Check number is QQ.
	 *
	 * @param int $num
	 * @return bool
	 */
	public static function isQQ($num)
	{
		if(empty($num) || filter_var($num, FILTER_VALIDATE_INT) === false)
		{
			return false;
		}

		return preg_match('#^[1-9][0-9]{4,12}$#', $num) ? $num : false;
	}

	/**
	 * Check string is color code.
	 *
	 * @param string $str
	 * @return bool
	 */
	public static function isColorCode($str)
	{
		if(empty($str))
		{
			return false;
		}

		return preg_match('#^\#[0-9a-f]{6}$#i', $str) ? $str : false;
	}

	/**
	 * Check string charset is UTF-8.
	 *
	 * @param string $str
	 * @return bool
	 */
	public static function isUtf8($str)
	{
		if(empty($str))
		{
			return false;
		}

		return preg_match("/^([" . chr(228) . "-" . chr(233) . "]{1}[" . chr(128) . "-" . chr(191) . "]{1}[" . chr(128) . "-" . chr(191) . "]{1}){1}/", $str) == true || preg_match("/([" . chr(228) . "-" . chr(233) . "]{1}[" . chr(128) . "-" . chr(191) . "]{1}[" . chr(128) . "-" . chr(191) . "]{1}){1}$/", $str) == true || preg_match("/([" . chr(228) . "-" . chr(233) . "]{1}[" . chr(128) . "-" . chr(191) . "]{1}[" . chr(128) . "-" . chr(191) . "]{1}){2,}/", $str) ? $str : false;
	}

	/**
	 * Check string is Chinese character.
	 *
	 * @param string $str
	 * @return bool
	 */
	public static function isChinese($str)
	{
		if(empty($str))
		{
			return false;
		}

		return preg_match('#[\4e00-\u9fa5]#', $str) ? $str : false;
	}

	/**
	 * Check string is JSON.
	 *
	 * @param string $str
	 * @return bool
	 */
	public static function isJson($str)
	{
		json_decode($str);

		return json_last_error() === JSON_ERROR_NONE ? true : false;
	}

	/**
	 * Check request is proxy.
	 *
	 * @return bool
	 */
	public static function isProxy()
	{
		if($_SERVER['HTTP_X_FORWARDED_FOR'] || $_SERVER['HTTP_VIA'] || $_SERVER['HTTP_PROXY_CONNECTION'] || $_SERVER['HTTP_USER_AGENT_VIA'])
		{
			return true;
		}

		return false;
	}

	/**
	 * Check request is robot.
	 *
	 * @return bool
	 */
	public static function isRobot()
	{
		if(preg_match("/(Bot|Crawl|Spider|slurp|sohu-search|lycos|robozilla)/", $_SERVER['HTTP_USER_AGENT']))
		{
			return true;
		}

		return false;
	}

	/**
	 * Check request is ajax.
	 *
	 * @return bool
	 */
	public static function isAjax()
	{
		return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') || (isset($_SERVER['X-Requested-With']) && $_SERVER['X-Requested-With'] == 'XMLHttpRequest') ? true : false;
	}

	/**
	 * Check request is https.
	 *
	 * @return bool
	 */
	public static function isHttps()
	{
		if(!empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off')
		{
			return true;
		}
		elseif(isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https')
		{
			return true;
		}
		elseif(!empty($_SERVER['HTTP_FRONT_END_HTTPS']) && strtolower($_SERVER['HTTP_FRONT_END_HTTPS']) !== 'off')
		{
			return true;
		}

		return false;
	}

	/**
	 * Check is command line.
	 *
	 * @return bool
	 */
	public static function isClient()
	{
		return (PHP_SAPI === 'cli' || defined('STDIN'));
	}

	/**
	 * Check OS is Windows.
	 *
	 * @return bool
	 */
	public static function isWindows()
	{
		return strpos(strtolower(PHP_OS), "win") !== false ? true : false;
	}
}