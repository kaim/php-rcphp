<?php
/**
 * Log class file.
 *
 * @author         RcPHP Dev Team
 * @copyright      Copyright (c) 2013,RcPHP Dev Team
 * @license        Apache License 2.0 {@link http://www.apache.org/licenses/LICENSE-2.0}
 * @package        Core
 * @since          1.0
 */
namespace RCPHP;

defined('IN_RCPHP') or exit('Access denied');

class Log
{

	/**
	 * 致命错误
	 */
	const FATAL = "fatal";

	/**
	 * 普通错误
	 */
	const ERROR = "error";

	/**
	 * 警告
	 */
	const WARNING = "warning";

	/**
	 * 信息
	 */
	const INFO = "info";

	/**
	 * 提示
	 */
	const NOTICE = "notice";

	/**
	 * 测试
	 */
	const DEBUG = "debug";

	/**
	 * Log根目录
	 *
	 * @var string
	 */
	private static $basePath = LOG_PATH;

	/**
	 * 默认Logger目录
	 *
	 * @var string
	 */
	private static $logger = 'Default';

	/**
	 * 记录日志级别
	 *
	 * @var array
	 */
	private static $level = array(
		"fatal",
		"error",
		"warning",
		"info",
		"notice",
		"critical",
		"debug"
	);

	/**
	 * 类型分割文件 默认关闭
	 *
	 * @var bool
	 */
	private static $isTypeFile = false;

	/**
	 * 小时分割文件 默认开启
	 *
	 * @var bool
	 */
	private static $isHourFile = true;

	/**
	 * 日志记录开关
	 *
	 * @var bool
	 */
	private static $isOpen = false;

	/**
	 * 初始化参数
	 *
	 * @return void
	 */
	public static function config()
	{
		$conf = \RCPHP\RcPHP::getConfig("log");

		if(!empty($conf))
		{
			self::$basePath = !empty($conf['default_basepath']) ? $conf['default_basepath'] : self::$basePath;
			self::$logger = !empty($conf['default_logger']) ? $conf['default_logger'] : self::$logger;
			self::$level = !empty($conf['level']) && is_array($conf['level']) ? $conf['level'] : self::$level;
			self::$isTypeFile = !empty($conf['type_file']) ? (bool)$conf['type_file'] : self::$isTypeFile;
			self::$isHourFile = !empty($conf['hour_file']) ? (bool)$conf['hour_file'] : self::$isHourFile;
		}

		/**
		 * 开启日志
		 */
		if(defined("RCPHP_LOG") && (bool)RCPHP_LOG !== false)
		{
			self::$isOpen = true;
		}
	}

	/**
	 * 设置/获取日志根目录
	 *
	 * @param string $basePath
	 * @return string
	 */
	public static function basePath($basePath = '')
	{
		if(!empty($basePath))
		{
			return self::$basePath = $basePath;
		}
		else
		{
			return self::$basePath;
		}
	}

	/**
	 * 设置/获取日志模块目录
	 *
	 * @param string $module
	 * @return string
	 */
	public static function logger($module = '')
	{
		if(!empty($module))
		{
			return self::$logger = $module;
		}
		else
		{
			return self::$logger;
		}
	}

	/**
	 * 记录致命错误日志
	 *
	 * @param string $message
	 * @param array  $content
	 * @param string $module
	 * @return bool
	 */
	public static function fatal($message, array $content = array(), $module = '')
	{
		return self::write(self::FATAL, $message, $content, $module);
	}

	/**
	 * 记录普通错误日志
	 *
	 * @param string $message
	 * @param array  $content
	 * @param string $module
	 * @return bool
	 */
	public static function error($message, array $content = array(), $module = '')
	{
		return self::write(self::ERROR, $message, $content, $module);
	}

	/**
	 * 记录警告日志
	 *
	 * @param string $message
	 * @param array  $content
	 * @param string $module
	 * @return bool
	 */
	public static function warning($message, array $content = array(), $module = '')
	{
		return self::write(self::WARNING, $message, $content, $module);
	}

	/**
	 * 记录信息日志
	 *
	 * @param string $message
	 * @param array  $content
	 * @param string $module
	 * @return bool
	 */
	public static function info($message, array $content = array(), $module = '')
	{
		return self::write(self::INFO, $message, $content, $module);
	}

	/**
	 * 记录提示日志
	 *
	 * @param string $message
	 * @param array  $content
	 * @param string $module
	 * @return bool
	 */
	public static function notice($message, array $content = array(), $module = '')
	{
		return self::write(self::NOTICE, $message, $content, $module);
	}

	/**
	 * 记录测试错误日志
	 *
	 * @param string $message
	 * @param array  $content
	 * @param string $module
	 * @return bool
	 */
	public static function debug($message, array $content = array(), $module = '')
	{
		return self::write(self::DEBUG, $message, $content, $module);
	}

	/**
	 * 记录日志
	 *
	 * @param string $errorType
	 * @param string $message
	 * @param array  $content
	 * @param string $module
	 * @return bool
	 */
	private static function write($errorType = self::DEBUG, $message, array $content = array(), $module = '')
	{
		if(!in_array($errorType, self::$level) || self::$isOpen === false)
		{
			return false;
		}

		$logFileName = self::$basePath . DS . (!empty($module) ? $module : self::$logger) . DS . date("Ym", time()) . DS;

		if(self::$isTypeFile === true)
		{
			$logFileName .= self::FATAL . '.' . date("Ymd", time());
		}
		else
		{
			$logFileName .= date("Ymd", time());
		}

		if(self::$isHourFile === true)
		{
			$logFileName .= ',' . date("H", time());
		}

		if(!empty($content))
		{
			$message = str_replace(array_keys($content), array_values($content), $message);
		}

		$content = $errorType . " | " . getmypid() . " | " . microtime(true) . " | " . date("Y-m-d H:i:s", time()) . " | " . \RCPHP\Net\Http::get_ip() . " | " . RcPHP::getController() . "." . RcPHP::getAction() . " | " . $message;

		return \RCPHP\Util\File::write($logFileName, $content . PHP_EOL, FILE_APPEND);
	}
}