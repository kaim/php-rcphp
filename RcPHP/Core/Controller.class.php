<?php
/**
 * Controller class file.
 *
 * @author         RcPHP Dev Team
 * @copyright      Copyright (c) 2013,RcPHP Dev Team
 * @license        Apache License 2.0 {@link http://www.apache.org/licenses/LICENSE-2.0}
 * @package        Core
 * @since          1.0
 */
namespace RCPHP;

defined('IN_RCPHP') or exit('Access denied');

abstract class Controller
{

	/**
	 * 视图实例对象
	 *
	 * @var null|object
	 */
	protected $view = null;

	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct()
	{
		if(get_magic_quotes_gpc())
		{
			$_POST = $this->stripSlashes($_POST);
			$_GET = $this->stripSlashes($_GET);
			$_COOKIE = $this->stripSlashes($_COOKIE);
		}

		$this->view = RcPHP::instance('\RCPHP\View');
	}

	/**
	 * Show error message.
	 *
	 * @param string $message
	 * @return bool
	 */
	public static function halt($message)
	{
		if(empty($message))
		{
			$message = "Unknow error.";
		}

		if(APP_DEBUG === true)
		{
			if(!is_array($message))
			{
				$trace = debug_backtrace();
				$e['message'] = $message;
				$e['file'] = $trace[0]['file'];
				$e['line'] = $trace[0]['line'];
				ob_start();
				debug_print_backtrace();
				$e['trace'] = ob_get_clean();
			}
			else
			{
				$e = $message;

				if(empty($e['trace']))
				{
					ob_start();
					debug_print_backtrace();
					$e['trace'] = ob_get_clean();
				}
			}

			if(\RCPHP\Util\Check::isClient())
			{
				exit(iconv('UTF-8', 'GBK', $e['message']) . PHP_EOL . 'FILE: ' . $e['file'] . '(' . $e['line'] . ')' . PHP_EOL . $e['trace']);
			}
			else
			{
				$e['trace'] = str_replace("\n", "</div><div>", "<div>" . $e['trace']);
				if(substr($e['trace'], -5) == '<div>')
				{
					$e['trace'] = substr($e['trace'], 0, strlen($e['trace']) - 5);
				}
				\RCPHP\Net\Http::send_http_status(404);
				include RCPHP_PATH . 'Tpl' . DS . 'exception.php';
				exit();
			}
		}
		else
		{
			\RCPHP\Net\Http::send_http_status(404);
			include RCPHP_PATH . 'Tpl' . DS . 'error.php';
			exit();
		}
	}

	/**
	 * Show prompt message.
	 *
	 * @param string $message
	 * @param string $gotoUrl
	 * @param int    $limitTime
	 * @return void
	 */
	public static function showMessage($message = '跳转中……', $gotoUrl = '-1', $limitTime = 5)
	{
		if(!is_null($gotoUrl))
		{
			if($gotoUrl == -1)
			{
				$gotoUrl = 'javascript:history.go(-1);';
			}
			else
			{
				$gotoUrl = str_replace(array(
					"\n",
					"\r"
				), '', $gotoUrl);
			}
		}

		include_once RCPHP_PATH . 'Tpl' . DS . 'message.php';

		exit();
	}

	/**
	 * URL redirect.
	 *
	 * @param string $url
	 * @return void|bool
	 */
	public function redirect($url, $time = 0, $msg = '')
	{
		if(empty($url))
		{
			return false;
		}

		$time = $time < 0 ? 0 : $time;

		if(empty($msg))
		{
			$msg = '系统将在' . $time . '秒之后为您跳转！';
		}

		$url = str_replace(array(
			"\r",
			"\n"
		), "", $url);

		if(!headers_sent())
		{
			if($time == 0)
			{
				header("location:" . $url);
			}
			else
			{
				header("refresh:" . $time . ";url=" . $url);
				echo($msg);
			}
			exit();
		}
		else
		{
			$str = "<meta http-equiv='refresh' content='" . intval($time) . ";URL=" . $url . "'>";
			if($time > 0)
			{
				$str .= $msg;
			}
			exit($str);
		}
	}

	/**
	 * Stripslashes.
	 *
	 * @param string|array $string
	 * @return string|array
	 */
	public function stripSlashes($string = null)
	{
		if(is_null($string))
		{
			return false;
		}

		if(!is_array($string))
		{
			return stripslashes($string);
		}

		foreach($string as $key => $value)
		{
			if(is_array($value))
			{
				$string[$key] = $this->stripSlashes($value);
			}
			else
			{
				$string[$key] = stripslashes($value);
			}
		}

		return $string;
	}

	/**
	 * Set the template variables.
	 *
	 * @param string|array $key
	 * @param string       $value
	 * @return $this
	 */
	public function assign($key, $value = null)
	{
		$this->view->assign($key, $value);

		return $this;
	}

	/**
	 * Display content template
	 *
	 * @param string $fileName
	 * @param array  $data
	 * @return void
	 */
	public function display($fileName = null, $data = null)
	{
		$this->view->display($fileName, $data);
	}

	/**
	 * Get the template content
	 *
	 * @param string $fileName
	 * @param array  $data
	 * @return string
	 */
	public function fetch($fileName = null, $data = array())
	{
		return $this->view->fetch($fileName, $data);
	}

	/**
	 * Build file.
	 *
	 * @param string $fileName
	 * @param string $suffix
	 * @param string $template
	 * @return string
	 */
	public function html($fileName, $suffix = '.html', $template = '')
	{
		$content = $this->fetch($template);

		\RCPHP\Util\File::write($fileName . $suffix, $template);

		return $content;
	}

	/**
	 * 析构函数
	 *
	 * @return void
	 */
	public function __destruct()
	{
		$this->view = null;
		Debug::addMessage("Action end");
	}
}